# convergentEvolution

A code base for:
### "A function agnostic test for molecular convergent evolution and discoveries in echolocating, aquatic and high-altitude mammals"
Amir Marcovitz, Yatish Turakhia, Heidi I Chen, Michael Gloudemans, Benjamin A Braun, Haoqing Wang & Gill Bejerano

doi: https://doi.org/10.1101/170985 


## Step 1. Run a scan for convergent and divergent sites (in conserved sites) for a selected pair of target species and their outgroups
For example, in echolocating mammalian scan (under BBLS>=0.9) run:

	./src/mammalianCodingConvergence.py --tg orcOrc1,turTru2:eptFus1,myoDav1,myoLuc2 --og bosTau8,capHir1,oviAri3,panHod1:pteAle1,pteVam1 --pc 0.9

where ':' seperates the lineages, and each lineage is a comma delimited list of assembly abbreviation of the species

the file speciesList.txt lists the species and their assembly abbreviations


## Step 2. Run enrichment test to discover if any pathway at all is enriched for coding convergent substitutions (or relaxation)
(No need to specify the outgroups)

For example:

	./src/enrichment_analysis.sh  dolphin,killer_whale  microbat,big_brown_bat,davids_myotis  0.9

### Requirements:
(i)  Install PAML (4.8 and up) in the src directory (where paml4.x is placed)

(ii) The code handles phylogenetic tree, using the tree_doctor tool from the Kent source (https://github.com/ucscGenomeBrowser/kent). Make sure it is installed, or replace with a similar tool (if so, edit lines 324,333,445 in src/ConvUtil.py) 

---
#### Copyright
© Bejerano lab (http://bejerano.stanford.edu/)
